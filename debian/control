Source: glibmm2.4
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Jeremy Bícha <jbicha@ubuntu.com>
Build-Depends: dpkg-dev (>= 1.22.5),
               debhelper-compat (= 13),
               doxygen,
               glib-networking <!nocheck>,
               graphviz,
               xsltproc,
               libglib2.0-dev (>= 2.61.2),
               libsigc++-2.0-dev (>= 2.9.1),
               libxml-parser-perl <!nodoc>,
               meson (>= 0.54.0),
               pkgconf,
               mm-common (>= 0.9.10)
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/gnome-team/glibmm
Vcs-Git: https://salsa.debian.org/gnome-team/glibmm.git
Homepage: https://www.gtkmm.org/

Package: libglibmm-2.4-1t64
Provides: ${t64:Provides}
X-Time64-Compat: libglibmm-2.4-1v5
Breaks: libglibmm-2.4-1v5 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Conflicts: libglibmm-2.4-1c2a
Replaces: libglibmm-2.4-1v5, libglibmm-2.4-1c2a
Description: C++ wrapper for the GLib toolkit (shared libraries)
 GLib is a low-level general-purpose library used mainly by GTK+/GNOME
 applications, but is useful for other programs as well.
 glibmm is the C++ wrapper for GLib.
 .
 This package contains shared libraries.

Package: libglibmm-2.4-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libglibmm-2.4-1t64 (= ${binary:Version}),
         libglib2.0-dev (>= 2.61.2),
         libsigc++-2.0-dev,
         pkgconf
Suggests: libglibmm-2.4-doc,
          libgtkmm-3.0-dev
Description: C++ wrapper for the GLib toolkit (development files)
 GLib is a low-level general-purpose library used mainly by GTK+/GNOME
 applications, but is useful for other programs as well.
 glibmm is the C++ wrapper for GLib.
 .
 This package contains development files.

Package: libglibmm-2.4-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         doc-base,
Suggests: devhelp, gtkmm-documentation
Description: C++ wrapper for the GLib toolkit (documentation)
 GLib is a low-level general-purpose library used mainly by GTK+/GNOME
 applications, but is useful for other programs as well.
 glibmm is the C++ wrapper for GLib.
 .
 This package contains reference documentation and examples.
